package org.example;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.jupiter.api.Test;

class EmployeeTest {

    @Test
    void shouldTestEarningLessThanExpected() throws Exception {
        String filename = "test_file1.csv";
        String expectedOutput = "Martin Chekov earns less than expected by 3800.0\n";
        assertEquals(expectedOutput, runSalaryCalculator(filename));
    }

    @Test
    void shouldTestEarningMoreThanExpected() throws Exception {
        String filename = "test_file2.csv";
        String expectedOutput = "Martin Chekov earns less than expected by 22000.0\n"
            + "Bob Ronstad earns more than expected by 2500.0\n";
        assertEquals(expectedOutput, runSalaryCalculator(filename));
    }

    @Test
    void shouldTestLongReportingLines() throws Exception {
        String filename = "test_file3.csv";
        String expectedOutput = "Michael Jordan has a reporting line that is too long, depth length is 1\n";
        assertEquals(expectedOutput, runSalaryCalculator(filename));
    }

    private static String runSalaryCalculator(String filename) throws Exception {
        ByteArrayOutputStream content = new ByteArrayOutputStream();
        System.setOut(new PrintStream(content));
        Main.main(new String[]{filename});
        return content.toString();
    }
}