package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.example.model.Employee;

public class Main {

    public static void main(String[] args) throws Exception {

        String fileName = args.length > 0 ? args[0] : "data.csv";
        List<Employee> employees = readAndMapFileToEmployees(fileName);
        Optional<Employee> ceo = getCeo(employees);
        if (ceo.isEmpty()) {
            System.err.println("Ceo not found");
            throw new Exception();
        }
        Map<Integer, List<Employee>> subordinatesMap = getSubordinateMapByManager(employees);

        /**
         * find and print salaries less than and more than expected for managers
         */
        findSalariesMoreOrLessThanExpected(employees, subordinatesMap);

        /**
         * Find and print Reporting which is more than 4 depth
         */
        findReportingLines(ceo.get(), subordinatesMap, 1);
    }

    /**
     * We are calculating lower and upper bounds based on average for all employees on this method
     * This solution is not the most efficient approach If this application is read heavy
     * Instead we can calculate this when persisting salary of each employee and persist it to employee record
     */
    private static void findSalariesMoreOrLessThanExpected(List<Employee> employees, Map<Integer, List<Employee>> subordinatesMap) {
        for (Employee employee : employees) {
            if (employee.getManagerId() != -1) {
                List<Employee> subordinates = subordinatesMap.getOrDefault(employee.getManagerId(), new ArrayList<>());
                double average = calculateAverageSalary(subordinates);
                double lowerBound = 0.8 * average;
                double upperBound = 1.5 * average;
                if (employee.getSalary() < lowerBound) {
                    System.out.println(
                        employee.getFullName() + " earns less than expected by " + (lowerBound - employee.getSalary()));
                } else if (employee.getSalary() > upperBound) {
                    System.out.println(
                        employee.getFullName() + " earns more than expected by " + (employee.getSalary() - upperBound));
                }
            }
        }
    }

    /**
     * Like above method, If this method is calling lots of times. And this is a read heavy system
     * we can again calculte this depth while persisting each employee.
     * @param employee
     * @param subordinatesMap
     * @param depth
     */
    private static void findReportingLines(Employee employee, Map<Integer, List<Employee>> subordinatesMap, int depth) {
        List<Employee> subordinates = subordinatesMap.getOrDefault(employee.getId(), new ArrayList<>());
        for (Employee subordinate : subordinates) {
            if (depth > 4) {
                System.out.println(
                    subordinate.getFullName() + " has a reporting line that is too long, depth length is " + (depth
                        - 4));
            }
            findReportingLines(subordinate, subordinatesMap, depth + 1);
        }
    }

    private static List<Employee> readAndMapFileToEmployees(String filename) {
        List<Employee> employees = new ArrayList<>();
        try (InputStream inputStream = Main.class.getResourceAsStream("/" + filename);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split(",");
                if (parts.length >= 4) {
                    int id = Integer.parseInt(parts[0]);
                    String firstName = parts[1];
                    String lastName = parts[2];
                    int salary = Integer.parseInt(parts[3]);
                    // I assume ceo has has managerId -1
                    int managerId = parts.length > 4 && !parts[4].isEmpty() ? Integer.parseInt(parts[4]) : -1;
                    employees.add(new Employee(id, firstName, lastName, salary, managerId));
                } else {
                    System.err.println("CSV file could not parsed: " + line);
                }
            }
        } catch (IOException e) {
            System.err.println("File Parse exception");
        }
        return employees;
    }

    private static Map<Integer, List<Employee>> getSubordinateMapByManager(List<Employee> employees) {
        Map<Integer, List<Employee>> map = new HashMap<>();
        for (Employee employee : employees) {
            if (employee.getManagerId() != -1) {
                map.computeIfAbsent(employee.getManagerId(), k -> new ArrayList<>()).add(employee);
            }
        }
        return map;
    }

    private static Optional<Employee> getCeo(List<Employee> employees) {
        return employees.stream().filter(employee -> employee.getManagerId() == -1).findAny();
    }

    private static double calculateAverageSalary(List<Employee> employees) {
        int total = 0;
        if (employees.size() == 0) {
            return 0.0;
        }
        for (Employee employee : employees) {
            total += employee.getSalary();
        }
        return total / employees.size();
    }

}