package org.example.model;

public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private int salary;
    private int managerId;

    public Employee(int id, String firstName, String lastName, int salary, int managerId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.managerId = managerId;
    }

    public int getId() {
        return id;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public int getSalary() {
        return salary;
    }

    public int getManagerId() {
        return managerId;
    }
}